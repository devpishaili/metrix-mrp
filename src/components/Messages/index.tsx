// FacebookMessages.tsx

import { useState } from 'react';
import MessageSection from './MessagesSection';
import MessageInput from './MessageInput';
import './style.css'; // Import the CSS file

interface Message {
  content: string;
  isSentByMe: boolean;
}

const Messages = () => {
  const [messages, setMessages] = useState<Message[]>([
    {
      content: 'Hello!',
      isSentByMe: false,
    },
    {
      content: 'Hi there!',
      isSentByMe: true,
    },
  ]);

  const handleSendMessage = (messageContent: string) => {
    if (messageContent.trim() !== '') {
      setMessages([
        ...messages,
        {
          content: messageContent,
          isSentByMe: true,
        },
      ]);
    }
  };

  return (
    <div className="messages">
      <MessageSection
        messageSections={[{ sender: 'User', messages: messages }]}
      />
      <MessageInput onSendMessage={handleSendMessage} />
    </div>
  );
};

export default Messages;
