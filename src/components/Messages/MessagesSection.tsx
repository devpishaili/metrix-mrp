// MessageSection.tsx

import React from 'react';
import MessageThread from './MessageThread';

interface Message {
  content: string;
  isSentByMe: boolean;
}

interface MessageSectionProps {
  messageSections: {
    sender: string;
    messages: Message[];
  }[];
}

const MessageSection: React.FC<MessageSectionProps> = ({ messageSections }) => {
  return (
    <div className="message-section">
      {messageSections.map((messageSection, index) => (
        <MessageThread key={index} messages={messageSection.messages} />
      ))}
    </div>
  );
};

export default MessageSection;
