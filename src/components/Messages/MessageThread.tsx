// MessageThread.tsx

import React from 'react';
import MessageBubble from './MessageBubble';

interface Message {
  content: string;
  isSentByMe: boolean;
}

interface MessageThreadProps {
  messages: Message[];
}

const MessageThread: React.FC<MessageThreadProps> = ({ messages }) => {
  return (
    <div className="message-thread">
      <div className="message-bubbles">
        {messages.map((message, index) => (
          <MessageBubble
            key={index}
            content={message.content}
            isSentByMe={message.isSentByMe}
          />
        ))}
      </div>
    </div>
  );
};

export default MessageThread;
