// MessageBubble.tsx

import React from 'react';

interface MessageBubbleProps {
  content: string;
  isSentByMe: boolean;
}

const MessageBubble: React.FC<MessageBubbleProps> = ({
  content,
  isSentByMe,
}) => {
  return (
    <div className={`message-bubble ${isSentByMe ? 'sent-by-me' : ''}`}>
      {content}
    </div>
  );
};

export default MessageBubble;
