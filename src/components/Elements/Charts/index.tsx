import { FC } from 'react';

interface ChartsProps {}

const Charts: FC<ChartsProps> = () => {
  return (
    <div className="h-80 w-80 border-[6rem] border-primary-blue-trans rounded-full relative">
      <div className="absolute top-[50%] left-[50%] -translate-x-[50%] -translate-y-[50%] h-64 w-64 border-[2rem] border-primary-blue rounded-full"></div>
    </div>
  );
};

export default Charts;
