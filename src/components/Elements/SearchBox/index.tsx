import { FC } from 'react';
import { FaSearch } from 'react-icons/fa';
import './style.css';

interface SearchBoxProps {}

const SearchBox: FC<SearchBoxProps> = () => {
  return (
    <div className="search-box">
      <FaSearch className="search-icon" />
      <input type="text" placeholder="Search..." className="search-input" />
    </div>
  );
};

export default SearchBox;
