import { FC } from 'react';
import formatCurrency from '../../utils/formatCurrency';

interface Order {
  id: number;
  customerName: string;
  orderDate: string;
  totalAmount: number;
}

interface OrdersTableProps {
  orders: Order[];
  onDeleteOrder: (orderId: number) => void;
}

const OrdersTable: FC<OrdersTableProps> = ({ orders, onDeleteOrder }) => {
  return (
    <table className="table">
      <thead>
        <tr>
          <th>ID</th>
          <th>Customer Name</th>
          <th>Order Date</th>
          <th>Total Amount</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {orders.map((order) => (
          <tr key={order.id}>
            <td>{order.id}</td>
            <td>{order.customerName}</td>
            <td>{order.orderDate}</td>
            <td>{formatCurrency(order.totalAmount, 'NGN', 'en-NG')}</td>
            <td>
              <button onClick={() => onDeleteOrder(order.id)}>Delete</button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default OrdersTable;
