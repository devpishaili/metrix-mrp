import { FC } from 'react';
import DefaultLayout from '../../layouts';
import SearchBox from '../../components/Elements/SearchBox';
import Messages from '../../components/Messages';

interface ConversationsPageProps {}

const ConversationsPage: FC<ConversationsPageProps> = () => {
  return (
    <DefaultLayout>
      <main className="px-4 py-4 h-full ">
        <div className="flex justify-between items-center">
          <h2>Conversations with customers</h2>

          <button className="">New Message</button>
        </div>

        <div className="flex gap-5">
          <div className="allConversations rounded-md w-full max-w-xs bg-white p-3">
            {/* Chat concersations header */}
            <div className="flex justify-between items-center">
              <h2>Contacts</h2>
              <div className="counts">32</div>
            </div>

            {/* Chat Search */}
            <SearchBox />
          </div>

          <div className="w-full h-full rounded-md bg-white">
            <Messages />
          </div>
        </div>
      </main>
    </DefaultLayout>
  );
};

export default ConversationsPage;
