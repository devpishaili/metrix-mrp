import { FC } from 'react';
import DefaultLayout from '../../layouts';

interface ProfilePageProps {}

const ProfilePage: FC<ProfilePageProps> = () => {
  return (
    <DefaultLayout>
      <main>
        <div>ProfilePage</div>
      </main>
    </DefaultLayout>
  );
};

export default ProfilePage;
