import { FC } from 'react';
import DefaultLayout from '../../layouts';

interface NotFoundPageProps {}

const NotFoundPage: FC<NotFoundPageProps> = () => {
  return (
    <DefaultLayout>
      <main className="px-6 py-4 h-full ">
        <div className="grid items-center text-center h-full">
          <h2 className="text-xl h-max">Oops! You seem to be lost.</h2>
          <a
            href="/"
            className="button bg-primary-blue text-white rounded-xl px-6 py-2 text-sm"
          >
            Back to Home
          </a>
        </div>
      </main>
    </DefaultLayout>
  );
};
export default NotFoundPage;
