import { FC, useState } from 'react';
import DefaultLayout from '../../layouts';
import OrdersTable from '../../components/Orders/OrdersTable';

import ordersData from '../../data/orders.json';

interface OrderPageProps {}

const OrderPage: FC<OrderPageProps> = () => {
  const [orders, setOrders] = useState<Product[]>(ordersData);

  const handleDeleteOrder = (orderId: number) => {
    setOrders(orders.filter((order) => order.id !== orderId));
  };

  return (
    <DefaultLayout>
      <main className="px-4 py-2">
        <OrdersTable orders={orders} onDeleteOrder={handleDeleteOrder} />
      </main>
    </DefaultLayout>
  );
};

export default OrderPage;
