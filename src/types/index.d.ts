interface Product {
  id: number;
  name: string;
  customerName: string;
  totalAmount: number;
  qty: number;
  orderDate: string;
  status: string;
  img: string;
}
