import {
  Route,
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider,
} from 'react-router-dom';
import Home from './pages/Home';
import OrderPage from './pages/Orders';
import NotFoundPage from './pages/NotFound';
import ConversationsPage from './pages/Conversations';

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route>
      <Route index element={<Home />} />
      <Route path="/orders" element={<OrderPage />} />
      <Route path="/conversations" element={<ConversationsPage />} />
      <Route path="*" element={<NotFoundPage />} />
    </Route>
  )
);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
