// Helper function to check if the link is active
const isActiveRoute = (path: string): boolean => {
  return location.pathname === path;
};

export default isActiveRoute;
