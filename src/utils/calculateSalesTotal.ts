function calculateSalesTotal(data: Product[]): number {
  let totalSales = 0;

  data.forEach((item) => {
    totalSales += item.totalAmount * item.qty;
  });

  return totalSales;
}

export default calculateSalesTotal;
