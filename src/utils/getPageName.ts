const getPageName = (path: string) => {
  const trimmedPath = path.trim();
  const pageName =
    trimmedPath === '/' ? 'Dashboard' : trimmedPath.split('/').pop() || '';
  return pageName.charAt(0).toUpperCase() + pageName.slice(1);
};

export default getPageName;
