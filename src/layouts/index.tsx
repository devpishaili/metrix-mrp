import { FC, ReactNode } from 'react';
import Header from '../components/Header';
import SideBar from '../components/SideBar';

interface DefaultLayoutProps {
  children: ReactNode;
}

const DefaultLayout: FC<DefaultLayoutProps> = ({ children }) => {
  return (
    <div className="flex w-full h-full min-h-screen">
      <SideBar />
      <div className="w-full">
        <Header />

        {children}
      </div>
    </div>
  );
};

export default DefaultLayout;
